const nodemailer = require('nodemailer');

const mailer = nodemailer.createTransport({
  host: process.env.NODEMAILER_HOST,
  port: process.env.NODEMAILER_PORT,
  secure: true,
  auth: {
    user: process.env.MAIL_SERVICE_USERNAME,
    pass: process.env.MAIL_SERVICE_PASSWORD,
  },
});

async function sendMail({ to, count }) {
  await mailer.sendMail(
    {
      from: process.env.MAIL_SERVICE_USERNAME,
      to,
      subject: 'Test notification',
      text: `You have ${count} unread notification${count > 1 ? "s" : ""}`,
    }
  );
};

module.exports = { sendMail };
