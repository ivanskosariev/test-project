const mongoose = require("mongoose");

const File = mongoose.model(
  "File",
  new mongoose.Schema({
    type: String,
    uri: String,
    size: String,
    filename: String,
    bucketFilename: String,
    uid: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Role"
    }
  })
);

module.exports = File;
