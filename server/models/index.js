const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.user = require("./user.model");
db.role = require("./role.model");
db.file = require("./file.model");
db.notification = require("./notification.model");

db.ROLES = ["user"];

module.exports = db;
