const db = require("../models");
const { sendMail } = require("../middlewares/mailer");

const Notification = db.notification;
const User = db.user;

const mailHandler = (uid) => {
  User.findById(uid, (err, user) => {
    if (!user.notificationType || user.notificationType === "instantly") {
      Notification.find({ uid, read: false }, (err, notifications) => {
        if (notifications.length > 0) sendMail({ to: user.email, count: notifications.length });
      });
    } else if (user.notificationType === "every10notifications") {
      Notification.find({ uid }, (err, notifications) => {
        if (notifications.length % 10 == 0) {
          const count = notifications.filter(notification => !notification.read).length;
          sendMail({ to: user.email, count });
        } 
      });
    }
  });
};

exports.getAll = (req, res) => {
  Notification.find({ uid: req.userId }, (err, notifications) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    } else res.status(200).send(notifications.reverse().map((notification) => ({
      id: notification._id,
      type: notification.type,
      read: notification.read,
      message: notification.message,
      uid: notification.uid,
    })));
  });
};

exports.create = async (req, res) => {
  const notification = new Notification({
    uid: req.userId,
    read: false,
    type: req.body.type,
    message: req.body.message,
  });

  notification.save((err, notification) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    } else {
      mailHandler(req.userId);
      res.status(200).send({
        message: "Notification successfully created",
        notification,
      });
    }
  });
};

exports.update = async (req, res) => {
  Notification.updateMany(
    { uid: req.userId, read: false },
    { $set: { read: true } },
    null,
    (err) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      } else {
        res.status(200).send({
          message: "Notifications successfully updated",
        });
      }
    },
  );
};
