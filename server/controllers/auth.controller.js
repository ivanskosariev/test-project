const db = require("../models");
const User = db.user;
const Role = db.role;

const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

exports.signup = (req, res) => {
  const user = new User({
    notificationType: "instantly",
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8)
  });

  user.save((err, user) => {
    const token = jwt.sign({ id: user.id }, process.env.APP_SECRET, {
      expiresIn: 86400 // 24 hours
    });

    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (req.body.roles) {
      Role.find(
        {
          name: { $in: req.body.roles }
        },
        (err, roles) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          user.roles = roles.map(role => role._id);
          user.save((err, user) => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }

            res.status(200).send({
              user: {
                id: user._id,
                username: user.username,
                notificationType: user.notificationType,
                email: user.email,
              },
              token,
            });
          });
        }
      );
    } else {
      Role.findOne({ name: "user" }, (err, role) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        user.roles = [role._id];
        user.save((err, user) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          res.status(200).send({
            user: {
              id: user._id,
              username: user.username,
              notificationType: user.notificationType,
              email: user.email,
            },
            token,
          });
        });
      });
    }
  });
};

exports.signin = (req, res) => {
  User.findOne({ email: req.body.email }, (err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (!user) {
      return res.status(404).send({ message: "User Not found." });
    }

    const passwordIsValid = bcrypt.compareSync(
      req.body.password,
      user.password
    );

    if (!passwordIsValid) {
      return res.status(401).send({
        accessToken: null,
        message: "Invalid Password!"
      });
    }

    const token = jwt.sign({ id: user.id }, process.env.APP_SECRET, {
      expiresIn: 86400 // 24 hours
    });

    res.status(200).send({
      user: {
        id: user._id,
        username: user.username,
        notificationType: user.notificationType,
        email: user.email,
      },
      token,
    });
  });
};

exports.verify = (req, res) => {
  User.findById(req.userId, (err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    res.status(200).send({
      id: user._id,
      username: user.username,
      notificationType: user.notificationType,
      email: user.email,
    });
  });
};

exports.update = (req, res) => {
  User.findByIdAndUpdate(
    req.userId,
    { $set: req.body },
    { new: true },
    (err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }

      res.status(200).send({
        id: user._id,
        username: user.username,
        notificationType: user.notificationType,
        email: user.email,
      });
    },
  );
};
