const db = require("../models");
const processFile = require("../middlewares/upload");
const { format } = require("util");
const { Storage } = require("@google-cloud/storage");

// Instantiate a storage client with credentials
const storage = new Storage({ keyFilename: process.env.STORAGE_KEY_FILE });
const bucket = storage.bucket(process.env.STORAGE_BUCKET_NAME);

const File = db.file;

exports.getAll = (req, res) => {
  File.find({ uid: req.userId }, (err, files) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    } else res.status(200).send(files.map((file) => ({
      id: file._id,
      type: file.type,
      uri: file.uri,
      size: file.size,
      filename: file.filename,
      uid: file.uid,
    })));
  });
};

exports.create = async (req, res) => {
  try {
    await processFile(req, res);

    if (!req.file) {
      return res.status(400).send({ message: "Please upload a file!" });
    }

    // Create a new blob in the bucket and upload the file data.


    const bucketFilename = `${Date.now()}.${req.file.originalname.split('.').pop()}`;
    const blob = bucket.file(bucketFilename);
    const blobStream = blob.createWriteStream({
      resumable: false,
    });

    blobStream.on("error", (err) => {
      res.status(500).send({ message: err.message });
    });

    blobStream.on("finish", async (data) => {
      // Create URL for directly file access via HTTP.
      const publicUrl = format(
        `https://storage.googleapis.com/${bucket.name}/${blob.name}`
      );

      const file = new File({
        uid: req.userId,
        filename: req.file.originalname,
        bucketFilename,
        type: req.file.mimetype,
        size: req.file.size,
        uri: publicUrl,
      });

      file.save((err, file) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        } else {
          res.status(200).send({
            message: "Uploaded the file successfully: " + file.filename,
            url: publicUrl,
          });
        }
      });
    });

    blobStream.end(req.file.buffer);
  } catch (err) {
    if (err.code == "LIMIT_FILE_SIZE") {
      return res.status(500).send({
        message: "File size cannot be larger than 2MB!",
      });
    }

    res.status(500).send({
      message: `Could not upload the file: ${req.file.originalname}. ${err}`,
    });
  }
};

exports.destroy = async (req, res) => {
  File.findById(req.params.id, async (err, file) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    } else {
      try {
        await bucket.file(file.bucketFilename).delete();
        file.delete();

        res.status(200).send({
          message: "File deleted successfully: " + file.filename,
        });
      } catch (err) {
        res.status(500).send({
          message: `Could not delete the file: ${file.filename}. ${err}`,
        });
      }
    }
  });
};
