require("dotenv").config();
const express = require("express");
const { sendMail } = require("./middlewares/mailer");
const cron = require('node-cron');
const db = require("./models");
const Role = db.role;
const User = db.user;
const Notification = db.notification;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

require('./routes/auth.routes')(app);
require('./routes/files.routes')(app);
require('./routes/notifications.routes')(app);

db.mongoose
  .connect(process.env.MONGODB_URI || "mongodb://mongo:27017/test-db", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
    initial();
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  });

app.get("/api", (req, res) => {
  res.json({ message: "Hello from server!" });
});

const PORT = process.env.API_PORT || 5001;

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});

function initial() {
  Role.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      new Role({
        name: "user"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'user' to roles collection");
      });
    }
  });

  cron.schedule('*/5 * * * *', () => {
    User.find({ notificationType: "every5minutes" }, (err, users) => {
      users.map(user => {
        Notification.find({ uid: user._id, read: false }, (err, notifications) => {
          if (notifications.length > 0) sendMail({ to: user.email, count: notifications.length });
        });
      });
    });
  });
};
