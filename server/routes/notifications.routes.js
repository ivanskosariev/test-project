const { authJwt } = require("../middlewares");
const controller = require("../controllers/notifications.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/notifications", [authJwt.verifyToken], controller.getAll);

  app.post("/api/notifications/create", [authJwt.verifyToken], controller.create);

  app.post("/api/notifications/update", [authJwt.verifyToken], controller.update);
};
