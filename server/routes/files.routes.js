const { authJwt } = require("../middlewares");
const controller = require("../controllers/files.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/files", [authJwt.verifyToken], controller.getAll);

  app.post("/api/files/create", [authJwt.verifyToken], controller.create);

  app.delete("/api/files/destroy/:id", [authJwt.verifyToken], controller.destroy);
};
