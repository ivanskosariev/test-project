## Configure env

First you need to create a file `.env` following the `.env.sample`.

You need to add credentials for the mailer (this is [nodemailer](https://www.npmjs.com/package/nodemailer), the login and password from the gmail and email from which letters will be sent will be enough).

```
MAIL_SERVICE_USERNAME=example@gmail.com
MAIL_SERVICE_PASSWORD=password
```

Also I have provided my credentials for Google Cloud Storage. Take a look at the `here-we-go-again.json` file.

## Run application

To run the application, you need to execute the following commands:

```
% docker compose build
% docker compose up
```

To unite the client and api on the same network, I used nginx. Therefore, the application runs on pure `localhost`.

## Completed features

* `Sign up` and `Sign in` forms
* a list of notifications with the ability to read them all
* upload, download and remove files
* email notifications
* the ability to configure email notifications (instantly, every 5 minutes, every 10 notifications)
