import axios from 'axios';

const instance = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL,
});

instance.interceptors.request.use((c) => {
  c.headers["x-access-token"] = localStorage.getItem('token');
  return c;
});

const api = {
  auth: {
    me: (data) => instance.post('api/auth/verify', data),
    login: (data) => instance.post('api/auth/signin', data),
    register: (data) => instance.post('api/auth/signup', data),
    update: (data) => instance.post('api/auth/update', data),
  },
  files: {
    getAll: () => instance.get('api/files'),
    create: (data) => instance.post('api/files/create', data),
    destroy: (id) => instance.delete(`api/files/destroy/${id}`),
  },
  notifications: {
    getAll: () => instance.get('api/notifications'),
    create: (data) => instance.post('api/notifications/create', data),
    update: () => instance.post('api/notifications/update'),
  }
};

export default api;
