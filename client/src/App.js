import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { observer } from 'mobx-react-lite';

import FilesContainer from './components/files/FilesContainer';
import SignIn from './components/auth/SignIn';
import SignUp from './components/auth/SignUp';
import Header from './components/Header';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';
import PrivateRoute from './privateRoute';
import rootStore from './stores/RootStore';
import { RootStoreProvider } from './stores/Context';

// Define the main app
const App = () => {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    rootStore.restore().then(() => setLoading(false));
  }, []);

  if (loading) return null;

  return (
    <RootStoreProvider value={rootStore}>
      <Router>
        <div className="App">
          <Header />
          <div className="container">
            <div>
              <PrivateRoute exact path="/" component={FilesContainer} />
              <Route path="/sign_in" component={SignIn} />
              <Route path="/sign_up" component={SignUp} />
            </div>
          </div>
        </div>
      </Router>
    </RootStoreProvider>
  );
};

export default observer(App);
