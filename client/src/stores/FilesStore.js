import { types } from 'mobx-state-tree';
import File from './models/File';
import FilesStoreActions from './actions/FilesStore';

const FilesStore = types
  .model('FilesStore', {
    list: types.optional(types.array(File), []),
  }).volatile(() => ({
    loading: false,
  })).actions(FilesStoreActions);

export default FilesStore;
