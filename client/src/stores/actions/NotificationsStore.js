import { flow, getEnv } from 'mobx-state-tree';

const NotificationsStoreActions = (self) => ({
  getAll: flow(function* getAll() {
    const { api } = getEnv(self);
    self.loading = true;
    try {
      const { data: notifications } = yield api.notifications.getAll();
      self.list = notifications;
    } catch (e) {
      console.error(e);
    }

    self.loading = false;
  }),

  create: flow(function* create(data) {
    const { api } = getEnv(self);
    yield api.notifications.create(data);
    yield self.getAll();
  }),

  update: flow(function* update() {
    const { api } = getEnv(self);
    yield api.notifications.update();
    yield self.getAll();
  }),
});

export default NotificationsStoreActions;
