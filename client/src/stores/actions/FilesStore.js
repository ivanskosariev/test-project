import { flow, getEnv } from 'mobx-state-tree';

const FilesStoreActions = (self) => ({
  getAll: flow(function* getAll() {
    const { api } = getEnv(self);
    self.loading = true;
    try {
      const { data: files } = yield api.files.getAll();
      self.list = files;
    } catch (e) {
      console.error(e);
    }

    self.loading = false;
  }),

  create: flow(function* create(data) {
    const { api } = getEnv(self);
    self.loading = true;
    yield api.files.create(data);
    yield self.getAll();
    self.loading = false;
  }),

  remove: flow(function* remove(id) {
    const { api } = getEnv(self);
    self.loading = true;
    yield api.files.destroy(id);
    yield self.getAll();
    self.loading = false;
  }),
});

export default FilesStoreActions;
