import { types } from 'mobx-state-tree';
import Notification from './models/Notification';
import NotificationsStoreActions from './actions/NotificationsStore';

const NotificationsStore = types
  .model('NotificationsStore', {
    list: types.optional(types.array(Notification), []),
  }).volatile(() => ({
    loading: false,
  })).actions(NotificationsStoreActions);

export default NotificationsStore;
