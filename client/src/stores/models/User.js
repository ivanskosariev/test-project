import { types } from 'mobx-state-tree';

const User = types.model('User', {
  id: types.optional(types.string, ''),
  username: types.optional(types.string, ''),
  email: types.optional(types.string, ''),
  notificationType: types.optional(types.string, ''),
});

export default User;
