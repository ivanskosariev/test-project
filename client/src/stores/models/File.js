import { types } from 'mobx-state-tree';

const File = types.model('File', {
  id: types.optional(types.string, ''),
  uid: types.optional(types.string, ''),
  type: types.optional(types.string, ''),
  uri: types.optional(types.string, ''),
  size: types.optional(types.string, ''),
  filename: types.optional(types.string, ''),
});

export default File;
