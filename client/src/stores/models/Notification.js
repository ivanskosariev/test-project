import { types } from 'mobx-state-tree';

const Notification = types.model('Notification', {
  id: types.optional(types.string, ''),
  uid: types.optional(types.string, ''),
  type: types.optional(types.string, ''),
  read: types.optional(types.boolean, false),
  message: types.optional(types.string, ''),
});

export default Notification;
