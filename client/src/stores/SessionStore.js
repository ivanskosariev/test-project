import { types } from 'mobx-state-tree';
import User from './models/User';
import SessionStoreActions from './actions/SessionStore';

const SessionStore = types
  .model('SessionStore', {
    user: types.maybeNull(User),
  })
  .views((self) => ({
    get isAuthenticated() {
      return Boolean(self.user);
    },
  })).actions(SessionStoreActions);

export default SessionStore;
