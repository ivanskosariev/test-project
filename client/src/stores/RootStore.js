import { types } from 'mobx-state-tree';
import { configure } from 'mobx';
import RootStoreActions from './actions/RootStore';

import SessionStore from './SessionStore';
import FilesStore from './FilesStore';
import NotificationsStore from './NotificationsStore';

import api from '../api/api';

configure({});

const RootStore = types
  .model('RootStore', {
    sessionStore: types.optional(SessionStore, () => SessionStore.create({})),
    filesStore: types.optional(FilesStore, () => FilesStore.create({})),
    notificationsStore: types.optional(NotificationsStore, () => NotificationsStore.create({})),
  }).actions(RootStoreActions);

const RootStoreInstance = RootStore.create({}, { api });

export { RootStoreInstance as default, RootStore };
