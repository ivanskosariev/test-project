import React, { useRef, useState, useEffect } from 'react';
import { MailOutline } from '@material-ui/icons';
import {
  FormControlLabel,
  Divider,
  Grow,
  IconButton,
  Paper,
  Popper,
  Radio,
  RadioGroup,
} from '@material-ui/core';
import { useStores } from '../stores/Context';

export default function Mailer() {
  const { sessionStore } = useStores();
  const [open, setOpen] = useState(false);
  const [type, setType] = useState(sessionStore.user.notificationType);

  const buttonRef = useRef(null);
  const popperRef = useRef(null);

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    const listener = event => {
      if (
        !popperRef.current ||
        popperRef.current.contains(event.target)
      ) return;
      handleClose();
    };

    document.addEventListener("mousedown", listener);
    document.addEventListener("touchstart", listener);
    return () => {
      document.removeEventListener("mousedown", listener);
      document.removeEventListener("touchstart", listener);
    };
  }, [popperRef, setOpen]);

  const handleSubmit = (notificationType) => {
    setType(notificationType);
    sessionStore.update({ notificationType });
  };

  return (
    <>
      <IconButton
        className="icon-btn"
        ref={buttonRef}
        onClick={() => setOpen(!open)}
        aria-label="Mailer"
        aria-haspopup="true"
      >
        <MailOutline fontSize="large" style={{ color: "#FFF" }} />
      </IconButton>
      <Popper
        ref={popperRef}
        open={open}
        anchorEl={buttonRef.current}
        role={undefined}
        placement="bottom-start"
        transition
        disablePortal
        style={{ zIndex: 1100, visibility: open ? 'visible' : 'hidden' }}
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
          >
            <Paper elevation={2}>
              <div className="Modal-container">
                <div className="Modal-title">
                  <span>Mailer Type</span>
                </div>
                <Divider />
                <div className="Modal-content">
                  <RadioGroup
                    aria-label="settings"
                    value={type}
                    name="radio-buttons-group"
                    onChange={(e) => handleSubmit(e.target.value)}
                  >
                    <FormControlLabel value="instantly" control={<Radio />} label="Instantly" />
                    <FormControlLabel value="every10notifications" control={<Radio />} label="Every 10 notifications" />
                    <FormControlLabel value="every5minutes" control={<Radio />} label="Every 5 minutes" />
                  </RadioGroup>
                </div>
              </div>
            </Paper>
          </Grow>
        )}
      </Popper>
    </>
  );
}
