import React, { useCallback, useRef, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import { useStores } from '../stores/Context';
import Notifications from './Notifications';
import Mailer from './Mailer';
import { Box, LinearProgress } from '@material-ui/core';

const Header = () => {
  const { sessionStore, filesStore, notificationsStore } = useStores();
  const { user, isAuthenticated } = sessionStore;

  const count = notificationsStore.list.filter(item => !item.read).length;

  const inputRef = useRef();

  const handleLogout = useCallback(() => {
    sessionStore.logout();
  }, []);

  const fetchNotifications = useCallback(() => {
    isAuthenticated && notificationsStore.getAll();
  }, [isAuthenticated]);

  const handleUpload = useCallback(async (file) => {
    const formData = new FormData();
    formData.append('file', file.target.files[0]);
    notificationsStore.create({
      message: `${file.target.files[0].name} upload started!`,
      type: "upload",
    });
    await filesStore.create(formData);
    notificationsStore.create({
      message: `${file.target.files[0].name} upload completed!`,
      type: "upload",
    });
  }, []);

  useEffect(fetchNotifications, [fetchNotifications]);

  return (
    <div className="App">
      {filesStore.loading &&
        <Box sx={{ width: '100%' }}>
          <LinearProgress />
        </Box>
      }
      <header className="App-header">
        {isAuthenticated ? (
          <div className="Header-content">
            <div className="Header-actions">
              <input ref={inputRef} type="file" hidden onChange={handleUpload} />
              <button type="button" className="btn btn-info mx-2" onClick={() => inputRef.current.click()}>
                Upload File
              </button>
            </div>
            <div className="Header-actions">
              <Mailer />
              <Notifications count={count} />
              <p>
                {'Hello, '}
                {user.username}
              </p>
              <button type="button" onClick={handleLogout} className="btn btn-danger mx-2">
                Logout
              </button>
            </div>
          </div>
        ) : (
          <div className="Header-content Center">
            <div className="Header-actions">
              <Link to="/sign_in" className="btn btn-light mx-2">
                Sign In
              </Link>
              <Link to="/sign_up" className="btn btn-light mx-2">
                Sign Up
              </Link>
            </div>
          </div>
        )}
      </header>
    </div>
  );
};

export default observer(Header);
