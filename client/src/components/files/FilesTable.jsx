import React, { useCallback, useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import {
  Description,
  Image,
  Movie,
  Audiotrack,
  Textsms,
} from "@material-ui/icons";
import { useStores } from '../../stores/Context';

const FilesTable = () => {
  const { filesStore, notificationsStore } = useStores();

  const fetchFiles = useCallback(() => {
    filesStore.getAll();
  }, []);

  const handleRemoveFile = useCallback((event, id, filename) => {
    event.preventDefault();
    filesStore.remove(id);
    notificationsStore.create({
      message: `${filename} removed!`,
      type: "delete",
    });
  }, []);

  const formatBytes = (bytes, decimals = 2) => {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  };

  const getTypeIcon = (type) => {
    switch (type.split('/')[0]) {
      case "image":
        return <Image />;
      case "video":
        return <Movie />;
      case "audio":
        return <Audiotrack />;
      case "text":
        return <Textsms />;
      case "application":
      default:
        return <Description />;
    }
  };

  useEffect(fetchFiles, [fetchFiles]);

  return (
    <div className="card">
      <div className="card-body">
        <table className="table table-hover table-responsive-sm">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">&nbsp;</th>
              <th scope="col">Name</th>
              <th scope="col">Size</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {
              filesStore.list.map((item, i) => (
                <tr key={item.id}>
                  <th scope="row">{i}</th>
                  <td>{getTypeIcon(item.type)}</td>
                  <td>{item.filename}</td>
                  <td>{formatBytes(item.size)}</td>
                  <td>
                    <div className="btn-group" role="group" aria-label="">
                      <button type="button" className="btn btn-danger" onClick={(e) => handleRemoveFile(e, item.id, item.filename)}>
                        Remove
                      </button>
                      <a className="btn btn-info" href={`data:,${item.uri}`} download={item.filename}>
                        Download
                      </a>
                    </div>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default observer(FilesTable);
