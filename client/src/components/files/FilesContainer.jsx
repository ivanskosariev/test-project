import React from 'react';
import { withRouter } from 'react-router-dom';
import FilesTable from './FilesTable';

const FilesContainer = () => (
  <div className="col mt-3">
    <FilesTable />
  </div>
);

export default withRouter(FilesContainer);
