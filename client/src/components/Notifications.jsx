import React, { useRef, useState, useEffect } from 'react';
import { NotificationsNone, AddAlert, CloudUpload, Delete } from '@material-ui/icons';
import { IconButton, Grow, Popper, Paper, Divider } from '@material-ui/core';
import { useStores } from '../stores/Context';

export default function Notifications(props) {
  const { count } = props;
  const [open, setOpen] = useState(false);
  const { notificationsStore } = useStores();
  
  const buttonRef = useRef(null);
  const popperRef = useRef(null);

  const handleClose = () => {
    setOpen(false);
  };

  const getTypeIcon = (type) => {
    switch (type.split('/')[0]) {
      case "upload":
        return <CloudUpload />;
      case "delete":
        return <Delete />;
      default:
        return <NotificationsNone />;
    }
  };

  useEffect(() => {
    const listener = event => {
      if (
        !popperRef.current ||
        popperRef.current.contains(event.target)
      ) return;
      handleClose();
    };

    document.addEventListener("mousedown", listener);
    document.addEventListener("touchstart", listener);
    return () => {
      document.removeEventListener("mousedown", listener);
      document.removeEventListener("touchstart", listener);
    };
  }, [popperRef, setOpen]);

  useEffect(() => {
    if (open && Boolean(count)) notificationsStore.update();
  }, [open, count]);

  return (
    <>
      <IconButton
        className="icon-btn"
        ref={buttonRef}
        onClick={() => setOpen(!open)}
        aria-label="Notifications"
        aria-haspopup="true"
      >
        {Boolean(count) && <div className="Counter">{count}</div>}
        <NotificationsNone fontSize="large" style={{ color: "#FFF" }} />
      </IconButton>
      <Popper
        ref={popperRef}
        open={open}
        anchorEl={buttonRef.current}
        role={undefined}
        placement="bottom-start"
        transition
        disablePortal
        style={{ zIndex: 1100, visibility: open ? 'visible' : 'hidden' }}
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
          >
            <Paper elevation={2}>
              <div className="Modal-container">
                <div className="Modal-title">
                  <span>Notifications</span>
                </div>
                <Divider />
                <div className="Modal-content">
                  {notificationsStore.list.map((notification) => (
                    <div className="Notifications-item" key={notification.id}>
                      {getTypeIcon(notification.type)}
                      <div className="Notifications-message">
                        {notification.message}
                      </div>
                    </div>
                  ))}
                  {!Boolean(notificationsStore.list.length) && (
                    <div className="Empty">
                      <AddAlert fontSize="large" />
                      <p>Your notifications appear here</p>
                    </div>
                  )}
                </div>
              </div>
            </Paper>
          </Grow>
        )}
      </Popper>
    </>
  );
}
