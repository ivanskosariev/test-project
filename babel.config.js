module.exports = {
  presets: [
    [
      "next/babel",
      {
        "transform-runtime": {
          absoluteRuntime: false,
        },
        "preset-react": {
          runtime: "automatic",
          importSource: "@emotion/react",
        },
      },
    ],
  ],
  plugins: ["@emotion/babel-plugin"],
};
